/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package blockcontract

import (
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/utils/v2"
)

// checkRoleAndFilterBlockTxs 检查用户有没有查询的权限
// @param block
// @param txSimContext
// @return *commonPb.Block
// @return error
func checkRoleAndFilterBlockTxs(block *commonPb.Block, txSimContext protocol.TxSimContext) (*commonPb.Block, error) {
	var (
		reqSender protocol.Role
		err       error
		ac        protocol.AccessControlProvider
	)

	ac, err = txSimContext.GetAccessControl()
	if err != nil {
		return block, err
	}

	reqSender, err = utils.GetRoleFromTx(txSimContext.GetTx(), ac)
	if err != nil {
		return block, err
	}

	reqSenderOrgId := txSimContext.GetTx().Sender.Signer.OrgId
	if reqSender == protocol.RoleLight {
		newBlock := utils.FilterBlockTxs(reqSenderOrgId, block)
		return newBlock, nil
	}

	return block, nil
}

// checkRoleAndGenerateTransactionInfo 验证是否有查询Tx的权限
// @param txSimContext
// @param transactionInfo
// @return *commonPb.TransactionInfoWithRWSet
// @return error
func checkRoleAndGenerateTransactionInfo(txSimContext protocol.TxSimContext,
	transactionInfo *commonPb.TransactionInfoWithRWSet) (*commonPb.TransactionInfoWithRWSet, error) {
	var (
		reqSender protocol.Role
		err       error
		ac        protocol.AccessControlProvider
	)
	tx := transactionInfo.Transaction

	if ac, err = txSimContext.GetAccessControl(); err != nil {
		return nil, err
	}

	if reqSender, err = utils.GetRoleFromTx(txSimContext.GetTx(), ac); err != nil {
		return nil, err
	}

	if reqSender == protocol.RoleLight {
		if tx.Sender.Signer.OrgId != txSimContext.GetTx().Sender.Signer.OrgId {
			//验证轻节点无权查看
			newTransactionInfo := &commonPb.TransactionInfoWithRWSet{
				Transaction: nil,
				BlockHeight: transactionInfo.BlockHeight,
				BlockHash:   transactionInfo.BlockHash,
				TxIndex:     transactionInfo.TxIndex,
				RwSet:       nil,
			}
			return newTransactionInfo, nil
		}
	}
	return transactionInfo, nil
}
